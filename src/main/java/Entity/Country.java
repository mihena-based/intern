package Entity;


import java.util.List;

public class Country {

    private String code; //Letter code example RR, EU
    private String name;
    private String continent;
    private double surfaceArea; //  1:1000 sqr km
    private int population;
    private String currency;
    private List<City> cities;

    private Country() {}

    public static CountryBuilder builder() {
        return new Country().new CountryBuilder();
    }

    public class CountryBuilder {
        private CountryBuilder() {
        }
        
        public CountryBuilder code(String code) {
            Country.this.code = code;
            return this;
        }

        public CountryBuilder name(String name) {
            Country.this.name = name;
            return this;
        }

        public CountryBuilder continent(String continent) {
            Country.this.continent = continent;
            return this;
        }
        public CountryBuilder surfaceArea(double surfaceArea) {
            Country.this.surfaceArea = surfaceArea;
            return this;
        }

        public CountryBuilder population(int population) {
            Country.this.population = population;
            return this;
        }

        public CountryBuilder currency(String currency) {
            Country.this.currency = currency;
            return this;
        }

        public CountryBuilder cities(List<City> cities) {
            Country.this.cities = cities;
            return this;
        }

        public Country build() {
            return Country.this;
        }
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getContinent() {
        return continent;
    }

    public double getSurfaceArea() {
        return surfaceArea;
    }

    public int getPopulation() {
        return population;
    }

    public String getCurrency() {
        return currency;
    }

    public List<City> getCities() {
        return cities;
    }

    @Override
    public String toString() {
        return "Country{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", continent='" + continent + '\'' +
                ", surfaceArea=" + surfaceArea +
                ", population=" + population +
                ", currency='" + currency + '\'' +
                ", cities=" + cities +
                '}';
    }
}
