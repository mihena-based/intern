package Entity;


import java.time.LocalDate;

public class City {
    private int id;
    private String name;
    private int population;
    private String countryCode;
    private LocalDate dateOfFoundation;
    
    private City() {}
    
    public static CityBuilder builder() {
        return new City().new CityBuilder();
    }
    
    public class CityBuilder {
        
        public CityBuilder id(int id) {
            City.this.id = id;
            return this;
        }

        public CityBuilder name(String name) {
            City.this.name = name;
            return this;
        }

        public CityBuilder population(int population) {
            City.this.population = population;
            return this;
        }

        public CityBuilder countryCode(String countryCode) {
            City.this.countryCode = countryCode;
            return this;
        }

        public CityBuilder dateOfFoundation(LocalDate dateOfFoundation) {
            City.this.dateOfFoundation = dateOfFoundation;
            return this;
        }

        public City build() {
            return City.this;
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPopulation() {
        return population;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public LocalDate getDateOfFoundation() {
        return dateOfFoundation;
    }

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", population=" + population +
                ", countryCode='" + countryCode + '\'' +
                ", dateOfFoundation=" + dateOfFoundation +
                '}';
    }
}
