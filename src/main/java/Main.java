import Entity.City;
import Entity.Country;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        final int CURRENT_YEAR = 2024;
        List<Country> countries = initCities();

        System.out.print("Task 1. African countries: ");
        countries.
                stream()
                .filter(country -> country.getContinent().equals("Africa"))
                .forEach(c -> System.out.print(c.getName() + ", "));
        System.out.print("\b\b\n");

        System.out.print("Task 2. Russian cities older than 400 y: ");
        countries
                .stream()
                .filter(country -> country.getName().equals("Russia"))
                .map(Country::getCities)
                .flatMap(Collection::stream)
                .filter(city -> CURRENT_YEAR - city.getDateOfFoundation().getYear() > 400)
                .forEach(city -> System.out.print(city.getName() + ", "));
        System.out.print("\b\b\n");

        System.out.print("Task 3. Countries with a population more than 100m: ");
        countries
                .stream()
                .filter(country -> country.getPopulation() > 100_000_000)
                .forEach(country -> System.out.print(country.getName() + ", "));
        System.out.print("\b\b\n");

        System.out.print("Task 4. (sum area, currency): ");

        System.out.printf("%.2f", countries
                .stream()
                .filter(country -> country.getCities().size() > 2)
                .peek(country -> System.out.print(country.getCurrency()+ ", "))
                .mapToDouble(Country::getSurfaceArea)
                .sum());
        System.out.println();

        System.out.print("Task 5. (dollar, area > 10000): ");
        countries
                .stream()
                .filter(country -> country.getCurrency().equalsIgnoreCase("USD"))
                .filter(country -> country.getSurfaceArea() > 1000) //10000? Не уверен насчёт входных данных, уменьшил в 10 раз, суть та же
                .forEach(country -> System.out.print(country.getName() + ", "));
        System.out.print("\b\b\n");

        System.out.print("Task 6. unique currency: ");
        countries
                .stream()
                .map(Country::getCurrency)
                .distinct()
                .forEach(currency -> System.out.print(currency + ", "));
        System.out.print("\b\b\n");

        System.out.print("Task 7. descending order of population: ");
        countries
                .stream()
                .sorted(Comparator.comparing(Country::getPopulation).reversed())
                .forEach(country -> System.out.print(country.getName() + ", "));
        System.out.print("\b\b\n");

        System.out.print("Task 8. 3 smallest country: ");
        countries
                .stream()
                .sorted(Comparator.comparing(Country::getSurfaceArea))
                .limit(3)
                .forEach(country -> System.out.print(country.getName() + ", "));
        System.out.print("\b\b\n");

        System.out.print("Task 9. 2nd european country: ");
        countries
                .stream()
                .filter(country -> country.getContinent().equalsIgnoreCase("Europe"))
                .skip(1)
                .forEach(country -> System.out.print(country.getName() + ", "));
        System.out.print("\b\b\n");

        System.out.print("Task 10. average population: ");
        long sum = countries
                .stream()
                .mapToInt(Country::getPopulation).sum();
        System.out.println(sum/countries.size());

        System.out.print("Task 11. sum of area: ");
        System.out.printf("%.2f", countries
                .stream()
                .mapToDouble(Country::getSurfaceArea)
                .sum());
        System.out.println();

        System.out.print("Task 12. group by continent: ");
        countries
                .stream()
                .map(Country::getContinent)
                .distinct()
                .forEach(continent -> System.out.print( continent + " "+
                        countries
                                .stream()
                                .filter(c -> c.getContinent().equalsIgnoreCase(continent))
                                .map(Country::getName).toList()+", "));
        System.out.println("\b\b");

        System.out.print("Task 13. group by city count: ");
        countries
                .stream()
                .map(country -> country.getCities().size())
                .distinct()
                .forEach(count -> System.out.print( count + " "+
                        countries
                                .stream()
                                .filter(c -> c.getCities().size() == count)
                                .map(Country::getName).toList()+", "));
        System.out.println("\b\b");
    }


    public static List<Country> initCities() {
        return List.of( //Initial Phase
                Country.builder() //Country RUSSIA
                        .code("RU")
                        .name("Russia")
                        .continent("Eurasia")
                        .surfaceArea(17098.246)
                        .population(146_150_789)
                        .currency("RUB")
                        .cities(List.of(
                                City.builder() //City OREL
                                        .id(1)
                                        .name("Orel")
                                        .population(296_633)
                                        .countryCode("RU")
                                        .dateOfFoundation(LocalDate.of(1566,1,1))
                                        .build(),
                                City.builder() //City Moscow
                                        .id(2)
                                        .name("Moscow")
                                        .population(13_149_803)
                                        .countryCode("RU")
                                        .dateOfFoundation(LocalDate.of(1147,1,1))
                                        .build(),
                                City.builder()
                                        .id(3)
                                        .name("Krasnodar")
                                        .population(1_138_654)
                                        .countryCode("RU")
                                        .dateOfFoundation(LocalDate.of(1793,1,1))
                                        .build() //City Krasnodar
                        ))
                        .build(),
                Country.builder() //Country MOROCCO
                        .code("MA")
                        .name("Morocco")
                        .continent("Africa")
                        .surfaceArea(710.850)
                        .population(37_112_080)
                        .currency("MAD")
                        .cities(List.of(
                                City.builder() //City Casablanca
                                        .id(4)
                                        .name("Casablanca")
                                        .population(3_356_337)
                                        .countryCode("MA")
                                        .dateOfFoundation(LocalDate.of(1770,1,1))
                                        .build(),
                                City.builder()
                                        .id(5)
                                        .name("Rabat")
                                        .population(1_884_917)
                                        .countryCode("MA")
                                        .dateOfFoundation(LocalDate.of(1146,1,1))
                                        .build(),
                                City.builder()
                                        .id(6)
                                        .name("Safi")
                                        .population(308_508)
                                        .countryCode("MA")
                                        .dateOfFoundation(LocalDate.of(1555,1,1))
                                        .build()
                        ))
                        .build(),
                Country.builder()
                        .code("US")
                        .name("United States of America")
                        .continent("North America")
                        .surfaceArea(9_800.000)
                        .population(334_914_895)
                        .currency("USD")
                        .cities(List.of(
                                City.builder()
                                        .id(7)
                                        .name("Dallas")
                                        .population(1_304_379)
                                        .countryCode("US")
                                        .dateOfFoundation(LocalDate.of(1841,1,1))
                                        .build(),
                                City.builder()
                                        .id(8)
                                        .name("Philadelphia")
                                        .population(6_096_120)
                                        .countryCode("US")
                                        .dateOfFoundation(LocalDate.of(1682,1,1))
                                        .build(),
                                City.builder()
                                        .id(9)
                                        .name("Washington")
                                        .population(689_545)
                                        .countryCode("US")
                                        .dateOfFoundation(LocalDate.of(1790,1,1))
                                        .build()
                        ))
                        .build(),
                Country.builder()
                        .code("NO")
                        .name("Norway")
                        .continent("Europe")
                        .surfaceArea(385.207)
                        .population(5_550_203)
                        .currency("NOK")
                        .cities(List.of(
                                City.builder()
                                        .id(10)
                                        .name("Oslo")
                                        .population(709_037)
                                        .countryCode("NO")
                                        .dateOfFoundation(LocalDate.of(1048,1,1))
                                        .build(),
                                City.builder()
                                        .id(11)
                                        .name("Sandvika")
                                        .population(115_543)
                                        .countryCode("NO")
                                        .dateOfFoundation(LocalDate.of(2004,6,4))
                                        .build()
                        ))
                        .build(),
                Country.builder()
                        .code("FR")
                        .name("France")
                        .continent("Europe")
                        .surfaceArea(643.801)
                        .population(68_373_443)
                        .currency("FRF")
                        .cities(List.of(
                                City.builder()
                                        .id(12)
                                        .name("Paris")
                                        .population(2_102_650)
                                        .countryCode("FR")
                                        .dateOfFoundation(LocalDate.of(-52,7,8))
                                        .build(),
                                City.builder()
                                        .id(13)
                                        .name("Liyon")
                                        .population(522_250)
                                        .countryCode("FR")
                                        .dateOfFoundation(LocalDate.of(-43,6,4))
                                        .build()
                        ))
                        .build(),
                Country.builder()
                        .code("EG")
                        .name("Egypt")
                        .continent("Africa")
                        .surfaceArea(1_001.450)
                        .population(106_221_670)
                        .currency("EGY")
                        .cities(List.of(
                                City.builder()
                                        .id(14)
                                        .name("Kair")
                                        .population(9_840_591)
                                        .countryCode("EG")
                                        .dateOfFoundation(LocalDate.of(969,7,8))
                                        .build()
                                ))
                        .build()
        );
    }
}
